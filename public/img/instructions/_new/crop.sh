files=(
    cue
    line_correct
    line_init
    line_response
    stim
)

# cropping info
# crops are made of screenshots on macbook non-fullscreen mode
# where the start of the display area is 440px from left and 386px from the top
# the size of the display area is 1000x600px
# because of macos scaling the screenshot area size is double, ie 2000x1200px

x=440
y=386
width=2000
height=1200
height_resize=150

# we want a smaller area so crop a smaller rectangle withing the area
ident_x=400
ident_y=150

x=$((x + ident_x))
y=$((y + ident_y))
width=$((width - ident_x * 2))
height=$((height - ident_y * 2))

for i in "${files[@]}"; do
    echo "$i"
    convert ${i}.png -colorspace gray -crop ${width}x${height}+${x}+${y} ${i}_.png
    convert ${i}_.png -colorspace gray -resize ${height_resize}x${height_resize}^ -contrast-stretch 15%%x60%% -unsharp 0x1 ${i}_.png
done

# cue needs different contrast
convert cue_.png -colorspace gray -unsharp 1x1+2 cue_sharp.png