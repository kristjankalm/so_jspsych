files=(
    distractor
    estimate
    gabor_multiple
    gabor_single
    line_multiple
    line_single
    response_correct
    target_multiple_gabor
    target_multiple
    target_single
)
for i in "${files[@]}"; do
    echo "$i"
    convert ${i}.png -colorspace gray -crop 550x410+445+430 ${i}.png
    convert ${i}.png -colorspace gray -resize 150x150^ -contrast-stretch 15%%x60%% -unsharp 0x1 ${i}.png
done
